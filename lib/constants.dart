import 'package:flutter/material.dart';

tileColor(int index, bool isCompleted) {
  if (isCompleted) {
    return Colors.green[100 * (index % 10) + 100];
  } else {
    if (index % 10 == 9) {
      return Colors.orange[300];
    } else {
      return Colors.orange[100 * (index % 10) + 100];
    }
  }
}

listItemStyle(bool isChecked) {
  return TextStyle(
      decoration: isChecked ? TextDecoration.lineThrough : null,
      decorationThickness: 2.85,
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontSize: 22);
}
