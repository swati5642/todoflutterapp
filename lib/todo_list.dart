import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:todo_app_flutter/constants.dart';
import 'package:todo_app_flutter/taskHandler/taskOperations.dart';
import 'package:todo_app_flutter/widget/addtaskwidget.dart';

import 'model/taskModel.dart';

class TodoTaskListScreen extends StatefulWidget {
  @override
  TodoTaskListScreenState createState() {
    return TodoTaskListScreenState();
  }
}

class TodoTaskListScreenState extends State<TodoTaskListScreen> {
  static const String _KeyId = "_id";
  static const String _KeyValue = "_value";
  static const String _KeyIsCompleted = "_isCompleted";
  static const String _KeyIndex = "_index";

  // Initialize task related operations
  TaskOperations taskOperations = TaskOperations();
  List<TaskModel> _taskList = [];

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void dispose() {
    super.dispose();
    _refreshController.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final title = 'Personal List';
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.add_circle_outline,
              color: Colors.white,
            ),
            onPressed: () {
              showAddTaskDialogue();
            },
          )
        ],
      ),
      body: StreamBuilder(
        stream: taskOperations.fetchTasksAsStream(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          // _taskList = getTaskListFromSnapshot(snapshot);
          // print("data ${getTaskListFromSnapshot(snapshot)}");
          if (snapshot.data != null &&
              snapshot.hasData &&
              snapshot.data.snapshot.value != null) {
            getTaskListFromSnapshot(snapshot);
            return getTaskListWidgets();
          } else {
            return addNewItemWidget();
          }
        },
      ),
    );
  }

  Widget getTaskListWidgets() {
    return Container(
      child: ReorderableListView(
        scrollDirection: Axis.vertical,
        onReorder: (int oldIndex, int newIndex) async {
          _updateItems(oldIndex, newIndex, _taskList[oldIndex].isCompleted);
        },
        children: List.generate(_taskList.length, (index) {
          return Dismissible(
            // UniqueKey for all the items
            key: Key(UniqueKey().toString()),
            onDismissed: (direction) async {
              if (direction == DismissDirection.startToEnd) {
                updateIndex();
                await taskOperations.reorderItem(
                    _taskList[index].id, index, 0, true);
              } else if (direction == DismissDirection.endToStart) {
                await taskOperations.removeTask(index, _taskList[index].id);
              }
            },
            background: Container(
              color: Colors.green,
              padding: EdgeInsets.symmetric(horizontal: 20),
              alignment: AlignmentDirectional.centerStart,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Complete',
                    style: listItemStyle(false),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Icon(Icons.check, color: Colors.black, size: 30),
                ],
              ),
            ),
            direction: _taskList[index].isCompleted
                ? null
                : DismissDirection.horizontal,
            secondaryBackground: Container(
                color: Colors.red,
                padding: EdgeInsets.symmetric(horizontal: 20),
                alignment: AlignmentDirectional.centerEnd,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Remove',
                      style: listItemStyle(false),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Icon(Icons.clear, color: Colors.black, size: 30),
                  ],
                )),
            child: ListTile(
              title: Text(
                _taskList[index].value,
                style: listItemStyle(_taskList[index].isCompleted),
              ),
              tileColor: tileColor(index, _taskList[index].isCompleted),
            ),
          );
        }),
      ),
    );
  }

  void _updateItems(int oldIndex, int newIndex, bool isComplete) {
    setState(
      () {
        if (newIndex > oldIndex) {
          newIndex -= 1;
        }
        TaskModel taskModel;
        for (int i = 0; i <= newIndex; i++) {
          if (i != oldIndex) {
            taskModel = _taskList[i];
            taskOperations.updateIndex(
                taskModel.displayIndex + 1, taskModel.id);
          } else {
            taskModel = _taskList[i];
            taskOperations.updateIndex(
                taskModel.displayIndex - newIndex, taskModel.id);
          }
        }
      },
    );
  }

  addNewItemWidget() {
    return Center(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          'To add your first task, click ',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          width: 10,
        ),
        IconButton(
          icon: Icon(
            Icons.add_circle_outline,
            size: 35,
            color: Colors.orange,
          ),
          onPressed: () {
            showAddTaskDialogue();
          },
        ),
      ],
    ));
  }

  Future<void> getTaskListFromSnapshot(AsyncSnapshot<dynamic> snapshot) async {
    _taskList.clear();
    Map<dynamic, dynamic> value = snapshot.data.snapshot.value;

    value?.forEach((key, values) {
      _taskList.add(TaskModel(values[_KeyId], values[_KeyValue],
          values[_KeyIsCompleted], values[_KeyIndex]));
    });
    if (_taskList.length > 1) {
      _taskList.sort((a, b) => b.compareTo(a));
    }
  }

  void updateIndex() {
    TaskModel taskModel;
    for (int i = 0; i < _taskList.length; i++) {
      taskModel = _taskList[i];
      taskOperations.updateIndex(taskModel.displayIndex + 1, taskModel.id);
    }
  }

  void showAddTaskDialogue() {
    showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: "Dialog",
      transitionDuration: Duration(milliseconds: 200),
      pageBuilder: (_, __, ___) {
        // widget implementation
        return AddTaskWidget(onTaskSubmitted: (String submittedText) async {
          // Open add new task dialog
          await taskOperations.addTask(
            _taskList.length,
            submittedText,
          );
          Navigator.pop(context);
        });
      },
    );
  }
}
