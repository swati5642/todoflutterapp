import 'package:firebase_database/firebase_database.dart';

class TaskModel implements Comparable {
  String _id;
  String _value;
  bool _isCompleted = false;
  int _displayIndex;

  TaskModel(this._id, this._value, this._isCompleted, this._displayIndex);

  String get value => _value;

  set value(String value) {
    _value = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  bool get isCompleted => _isCompleted;

  set isCompleted(bool value) {
    _isCompleted = value;
  }

  int get displayIndex => _displayIndex;

  set displayIndex(int value) {
    _displayIndex = value;
  }

  @override
  int compareTo(other) {
    if (this._displayIndex == null || other == null) {
      return null;
    }

    if (this.displayIndex > other.displayIndex) {
      return 1;
    }

    if (this.displayIndex < other.displayIndex) {
      return -1;
    }

    if (this.displayIndex == other.displayIndex) {
      if (this.isCompleted) {
        return 1;
      }
      return 0;
    }
    return null;
  }
}
