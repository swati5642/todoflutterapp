import 'package:flutter/material.dart';
import 'package:todo_app_flutter/todo_list.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark()
          .copyWith(primaryColor: Colors.black, accentColor: Colors.orange),
      home: TodoTaskListScreen(),
      routes: {
        'todoList': (context) => TodoTaskListScreen(),
      },
    );
  }
}
