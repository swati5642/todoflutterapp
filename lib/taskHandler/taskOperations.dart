import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:todo_app_flutter/model/taskModel.dart';

import '../model/taskModel.dart';

class TaskOperations {
  static const String _KeyId = "_id";
  static const String _KeyValue = "_value";
  static const String _KeyIsCompleted = "_isCompleted";
  static const String _KeyIndex = "_index";

  DatabaseReference _databaseRef;
  List<TaskModel> _taskList = [];

  TaskOperations() {
    _databaseRef = FirebaseDatabase.instance.reference().child("task");
  }

  Stream<Event> fetchTasksAsStream() {
    return _databaseRef.onValue;
  }

  Future addTask(int index, String taskName) async {
    // Storing unique key for child and key for each item
    String keyId = UniqueKey().hashCode.toString();
    await _databaseRef.child(keyId).set(
      {
        _KeyId: keyId,
        _KeyValue: taskName,
        _KeyIsCompleted: false,
        _KeyIndex: index
      },
    ).catchError((onError) {
      print(onError);
    });
    return;
  }

  Future removeTask(int index, String childId) async {
    // Remove from database
    await _databaseRef.child(childId).remove();
    return;
  }

  Future reorderItem(
      String childId, int oldIndex, int newIndex, bool isCompleted) async {
    // Set completion status for task
    await _databaseRef
        .child(childId)
        .update({_KeyIsCompleted: isCompleted, _KeyIndex: newIndex});
  }

  Future updateIndex(int index, String childId) async {
    // Set index for task
    await _databaseRef.child(childId).update({_KeyIndex: index});
  }
}
