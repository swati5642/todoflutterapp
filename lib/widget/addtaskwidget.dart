import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddTaskWidget extends StatelessWidget {
  final Function onTaskSubmitted;
  AddTaskWidget({this.onTaskSubmitted});

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
      // makes widget fullscreen
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.orange.withOpacity(0.60),
          body: Container(
            height: double.infinity,
            child: TextField(
                textCapitalization: TextCapitalization.sentences,
                cursorColor: Color(0xFFbdc6cf),
                autofocus: true,
                style: TextStyle(
                    fontSize: 24.0,
                    color: Color(0xFFbdc6cf),
                    fontWeight: FontWeight.bold),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.orange,
                  hintText: 'Enter new task',
                  contentPadding: const EdgeInsets.only(
                      left: 14.0, bottom: 20.0, top: 20.0),
                ),
                onSubmitted: (String text) {
                  onTaskSubmitted(text);
                }),
          ),
        ),
      ),
    );
  }
}
